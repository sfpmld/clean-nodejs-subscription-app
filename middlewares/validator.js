const validate = require('validatorHelperPath');

// Middleware Validation factory
module.exports = {
  validationMiddleware: function (model, scope) {

    return (req, res, next) => {
      const validationResult = validate(model, req.body, scope);
      if(validationResult.error) {
        throw new Error(validationResult.error.message);
      } else {
        next();
      }
    }
  },
};
