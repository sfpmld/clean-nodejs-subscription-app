const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const helmet = require('helmet');


const commonMiddlewares = app => {

  // Common Middlewares
  app.use(bodyParser());
  app.use(morgan('common'));
  app.use(cors());
  app.use(helmet());

}

module.exports = commonMiddlewares;
