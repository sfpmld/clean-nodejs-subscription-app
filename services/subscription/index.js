const makeListSubscription = require('./listSubscription');
const makeAddSubscription = require('./addSubscription');
const makeSubscriptionById = require('./subscriptionById');
const makeRemoveSubscription = require('./removeSubscription');

const { SubscriptionDb } = require('../../data-access');


const listSubscription = makeListSubscription({ SubscriptionDb });
const addSubscription = makeAddSubscription({ SubscriptionDb });
const subscriptionById = makeSubscriptionById({ SubscriptionDb });
const removeSubscription = makeRemoveSubscription({ SubscriptionDb });


module.exports = Object.freeze({
  listSubscription,
  addSubscription,
  subscriptionById,
  removeSubscription
});

