module.exports = function makeRemoveSubscription ({ SubscriptionDb }) {
  return async function RemoveSubscription ({ id } = {}) {

    const result = await SubscriptionDb.deleteOne(id)

    return result;
  }
}
