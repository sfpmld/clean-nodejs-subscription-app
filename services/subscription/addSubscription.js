const { makeSubscription } = require('../../entities')

module.exports = function makeAddSubscription ({ SubscriptionDb }) {
  return async function addSubscription (data = {}) {
    debugger
    let Subscription = makeSubscription(data);

    return await SubscriptionDb.create({
      id: Subscription.getId(),
      planId: Subscription.getPlanId(),
      coupon: Subscription.getCoupon(),
      cardNumber: Subscription.getCardNumber(),
      holderName: Subscription.getHolderName(),
      expirationDate: Subscription.getExpirationDate(),
      cvv: Subscription.getCvv(),
      userId: Subscription.getUserId(),
      createdAt: Subscription.getCreatedAt(),
      updatedAt: Subscription.getUpdatedAt()
    });
  };
};
