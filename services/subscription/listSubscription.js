module.exports = function makeListSubscription ({ SubscriptionDb }) {
  return async function listSubscription ({ postId } = {}) {

    const results = await SubscriptionDb.findAll(postId)

    return results;

  }
}
