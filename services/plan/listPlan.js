module.exports = function makeListPlan ({ PlanDb }) {
  return async function listPlan ({ postId } = {}) {

    const results = await PlanDb.findAll(postId)

    return results;

  }
}
