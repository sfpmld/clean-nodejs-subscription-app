const makeListPlan = require('./listPlan');
const makeAddPlan = require('./addPlan');
const makePlanById = require('./planById');
const makeRemovePlan = require('./removePlan');

const { PlanDb } = require('../../data-access');


const listPlan = makeListPlan({ PlanDb });
const addPlan = makeAddPlan({ PlanDb });
const planById = makePlanById({ PlanDb });
const removePlan = makeRemovePlan({ PlanDb });


module.exports =  Object.create({
  listPlan,
  addPlan,
  planById,
  removePlan
});

