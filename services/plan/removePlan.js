module.exports = function makeRemovePlan ({ PlanDb }) {
  return async function removePlan ({ id } = {}) {

    const result = await PlanDb.deleteOne(id)

    return result;
  }
}
