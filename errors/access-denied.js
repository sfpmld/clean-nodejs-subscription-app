module.exports.AccessDeniedError = class AccessDeniedError {
  constructor(message) {
    this.message = message;
  }
}
