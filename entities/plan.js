module.exports = function buildMakePlan ({ Id, validate }) {
  return function makePlan ({
    id = Id(),
    name,
    price,
    type,
    userId,
    createdAt = Date.now(),
    updatedAt = Date.now()
  } = {}) {

    const validationResult = (function() {
      return validate(
        'Plan',
        {
          name,
          price,
          type,
          userId,
        },
        'default'
      );
    })();

    if (validationResult.error) {
      throw new Error(validationResult.error.message);
    }

    return Object.freeze({
      getId: () => id,
      getName: () => name,
      getPrice: () => price,
      getType: () => type,
      getUserId: () => userId,
      getCreatedAt: () => createdAt,
      getUpdatedAt: () => updatedAt
    })

  }
}
