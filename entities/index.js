const validate = require('../helpers/validate');
const Id = require('../utils/makeId');
const buildMakePlan = require('./plan');
const buildMakeSubscription = require('./subscription');


const makePlan = buildMakePlan({ Id, validate });
const makeSubscription = buildMakeSubscription({ Id, validate });

module.exports = {
  makePlan,
  makeSubscription,
};
