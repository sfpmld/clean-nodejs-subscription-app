module.exports = function buildMakeSubscription ({ Id, validate }) {

  return function makeSubscription ({
    id = Id(),
    planId,
    coupon,
    cardNumber,
    holderName,
    expirationDate,
    cvv,
    userId,
    createdAt = Date.now(),
    updatedAt = Date.now()
  } = {}) {


    const validationResult = (function() {
      return validate(
        'Subscription',
        {
          planId,
          coupon,
          cardNumber,
          holderName,
          expirationDate,
          cvv,
          userId,
        },
        'default'
      );
    })();

    if (validationResult.error) {
      throw new Error(validationResult.error.message);
    };

    return Object.freeze({
      getId: () => id,
      getPlanId: () => planId,
      getCoupon: () => coupon,
      getCardNumber: () => cardNumber,
      getHolderName: () => holderName,
      getExpirationDate: () => expirationDate,
      getCvv: () => cvv,
      getUserId: () => userId,
      getCreatedAt: () => createdAt,
      getUpdatedAt: () => updatedAt
    });
  };
};
