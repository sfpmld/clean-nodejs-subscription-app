const makeExpressCallback = require('../helpers/express-callback');
const {
  getSubscriptionList,
  getSubscriptionById,
  createSubscription,
  deleteSubscription,
  notFound
} = require('../controllers');


const router = require('express').Router();

router.get('/', makeExpressCallback(getSubscriptionList));

// get subscriptionRoutes Requests
router.get('/:id', makeExpressCallback(getSubscriptionById));

// post subscriptionRoutes Requests
router.post('/', makeExpressCallback(createSubscription));

// delete subscriptionRoutes Requests
router.delete('/:id', makeExpressCallback(deleteSubscription));

// notFound
router.use('*', makeExpressCallback(notFound));


module.exports = router;
