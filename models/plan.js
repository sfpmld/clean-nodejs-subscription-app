const Joi = require('@hapi/joi');

'use strict';

const PlanValidationSchema = Joi.object().keys({
  name: Joi.string().required(),
  price: Joi.number().positive().allow(0).required(),
  type: Joi.string().valid("monthly", "yearly").required(),
  userId: Joi.string().guid().required()
});

const Plan = (sequelize, DataTypes) => {
  const Plan = sequelize.define('Plan', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
    }
  }, {});
  Plan.associate = function(models) {
    // associations can be defined here
  };
  return Plan;
};

module.exports = Object.freeze({
  PlanValidationSchema,
  Plan,
})
