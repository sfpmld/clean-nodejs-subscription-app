const {
  listPlan,
  addPlan,
  planById,
  removePlan,
} = require('../services/plan');

const notFound = require('./notFound');

const {
  listSubscription,
  addSubscription,
  subscriptionById,
  removeSubscription,
} = require('../services/subscription');

const makeGetPlanList = require('./plan/getPlanList');
const makeCreatePlan = require('./plan/createPlan');
const makeGetPlanById = require('./plan/getPlanById');
const makeDeletePlan = require('./plan/deletePlan');

const makeGetSubscriptionList = require('./subscription/getSubscriptionList');
const makeCreateSubscription = require('./subscription/createSubscription');
const makeGetSubscriptionById = require('./subscription/getSubscriptionById');
const makeDeleteSubscription = require('./subscription/deleteSubscription');

const getPlanList = makeGetPlanList({ listPlan });
const createPlan = makeCreatePlan({ addPlan });
const getPlanById = makeGetPlanById({ planById });
const deletePlan = makeDeletePlan({ removePlan });

const getSubscriptionList = makeGetSubscriptionList({ listSubscription });
const createSubscription  = makeCreateSubscription({ addSubscription });
const getSubscriptionById = makeGetSubscriptionById({ subscriptionById });
const deleteSubscription = makeDeleteSubscription({ removeSubscription });


module.exports = Object.create({
  // plan
  getPlanList,
  createPlan,
  getPlanById,
  deletePlan,
  // subscription
  getSubscriptionList,
  createSubscription,
  getSubscriptionById,
  deleteSubscription,
  // not found
  notFound,
});

