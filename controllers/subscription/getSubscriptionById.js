const {
  HTTP_OK,
  HTTP_BAD_REQUEST
} = require('../../nomenclatures/httpStatusCode');

module.exports = function makeGetSubscriptionById ({ subscriptionById }) {
  return async function getSubscriptionById (httpRequest) {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      debugger
      const result = await subscriptionById(httpRequest.params);
      return {
        headers,
        statusCode: HTTP_OK,
        body: result
      }
    } catch (e) {
      console.log(e)
      return {
        headers,
        statusCode: HTTP_BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}
