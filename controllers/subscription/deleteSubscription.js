const {
  HTTP_OK,
  HTTP_BAD_REQUEST
} = require('../../nomenclatures/httpStatusCode');

module.exports = function makeDeleteSubscription ({ removeSubscription }) {
  return async function deleteSubscription (httpRequest) {
    debugger
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const result = await removeSubscription(httpRequest.params)
      return {
        headers,
        statusCode: HTTP_OK,
        body: result
      }
    } catch (e) {
      console.log(e)
      return {
        headers,
        statusCode: HTTP_BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}
