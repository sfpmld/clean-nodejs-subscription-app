const {
  HTTP_OK,
  HTTP_BAD_REQUEST
} = require('../../nomenclatures/httpStatusCode');

module.exports = function makeGetSubscriptionList ({ listSubscription }) {
  return async function getSubscriptionList (httpRequest) {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const result = await listSubscription({ httpRequest })
      return {
        headers,
        statusCode: HTTP_OK,
        body: result
      }
    } catch (e) {
      console.log(e)
      return {
        headers,
        statusCode: HTTP_BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}
