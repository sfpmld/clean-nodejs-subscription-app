const { HTTP_NOT_FOUND } = require('../nomenclatures/httpStatusCode');

module.exports = async function notFound () {
  return {
    headers: {
      'Content-Type': 'application/json'
    },
    body: { error: 'Not found.' },
    statusCode: HTTP_NOT_FOUND
  };
};
