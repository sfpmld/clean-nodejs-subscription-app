const Id = require('../utils/makeId');


module.exports = function makeSubscriptionDb(makeDb) {
  const db = makeDb();

  const findAll = function() {
    return db.Subscription.findAll();
  };

  const findOne = function(id) {
    return db.Subscription.findOne(id)
  }

  const create = function(data) {
    return db.Subscription.create(data)
  }

  const deleteOne = async function(id) {

    const subscriptionToDelete = await db.Subscription.findOne({ id })
    return await subscriptionToDelete.destroy();
  }

  return Object.freeze({
    findAll,
    findOne,
    create,
    deleteOne
  });
};




