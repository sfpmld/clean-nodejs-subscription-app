const makeDbConnection = require('./db/connection');
const { Sequelize, SequelizeClient } = makeDbConnection();

const makePlanModel = require('../models').Plan;
const makeSubscriptionModel = require('../models').Subscription;

const makePlanDb = require('./planDb');
const makeSubscriptionDb = require('./subscriptionDb');

const makeDb = () => {
  Plan = makePlanModel(SequelizeClient, Sequelize);
  Subscription = makeSubscriptionModel(SequelizeClient, Sequelize);

  return Object.freeze({
    Plan,
    Subscription
  })
}

const PlanDb = makePlanDb(makeDb);
const SubscriptionDb = makeSubscriptionDb(makeDb);

module.exports = Object.freeze({
  PlanDb,
  SubscriptionDb
});
