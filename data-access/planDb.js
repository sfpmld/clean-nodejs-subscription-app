const Id = require('../utils/makeId');


module.exports = function makePlanDb(makeDb) {
  const db = makeDb();

  const findAll = function() {
    return db.Plan.findAll();
  };

  const findOne = function(id) {
    return db.Plan.findOne(id)
  }

  const create = function(data) {
    return db.Plan.create(data)
  }

  const deleteOne = async function(id) {
    const planToDelete = await db.Plan.findOne({ id })
    return await planToDelete.destroy();
  }

  return Object.freeze({
    findAll,
    findOne,
    create,
    deleteOne
  });
};



